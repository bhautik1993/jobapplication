@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('success'))
                 <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if(Session::has('error'))
                 <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">Job Application List</div>

                <div class="card-body">
                    <table class="table table-bordered" id="datatable1">
                        <thead>
                            <tr>
                                <th>Sr.No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Designation</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jobApplicationDB as $jobApplication)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$jobApplication->first_name." ".$jobApplication->last_name}}</td>
                                <td>{{$jobApplication->email}}</td>
                                <td>{{$jobApplication->phone_number}}</td>
                                <td>{{$jobApplication->apply_designation}}</td>
                                <td>
                                    <form method="POST" id="" action="{{ route('deleteJob',$jobApplication->id) }}" onsubmit="return confirm('Are you sure you want to delete?')">
                                        <input type="hidden" name="_method" value="DELETE">
                                    @csrf
                                        <a href="{{route('viewJob',$jobApplication->id)}}" class="btn btn-primary btn-sm">View</a>
                                        <a href="{{route('editJob',$jobApplication->id)}}" class="btn btn-success btn-sm">Edit</a>
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
