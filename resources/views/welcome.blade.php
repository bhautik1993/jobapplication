@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Job Application Form</h2>
        </div>
    </div>
    <form method="POST" id="jobApplicationForm" action="{{ route('jobPost') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(Session::has('success'))
                     <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">Basic Details</div>

                    <div class="card-body">
                       
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" required="" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Designation</label>
                                    <input type="text" name="apply_designation" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone_number" required="" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address 1</label>
                                    <input type="text" name="address_1" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address 2</label>
                                    <input type="text" name="address_2" required="" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <select class="form-control" name="state" required>
                                        <option>Gujarat</option>
                                        <option>Maharastra</option>
                                        <option>Haryana</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Zipcode</label>
                                    <input type="text" name="zipcode" required="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div>
                                        <div class="form-check-inline">
                                          <label class="form-check-label" for="radio1">
                                            <input type="radio" class="form-check-input" id="radio1" name="gender" value="Male" checked>Male
                                          </label>
                                        </div>
                                        <div class="form-check-inline">
                                          <label class="form-check-label" for="radio2">
                                            <input type="radio" class="form-check-input" id="radio2" name="gender" value="Female">Female
                                          </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>RelationShip Status</label>
                                    <select class="form-control" name="relation_status" required>
                                        <option>Single</option>
                                        <option>Marride</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date Of Birth</label>
                                    <input type="text" readonly="" name="dob" required="" class="form-control datepicker">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Education Details</div>

                    <div class="card-body">
                     
                        <div class="row">
                            <div class="col-md-12">
                                <h4>SSC Result</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name Of Board</label>
                                    <input type="hidden" name="ssc_course_name" value="SSC" class="form-control">
                                    <input type="text" name="ssc_board_university" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Passing Year</label>
                                    <input type="text" name="ssc_passing_year" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Percentage</label>
                                    <input type="text" name="ssc_percentage" required="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4>HSC/Deploma Result</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name Of Board</label>
                                    <input type="hidden" name="hsc_course_name" value="HSC" class="form-control">
                                    <input type="text" name="hsc_board_university" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Passing Year</label>
                                    <input type="text" name="hsc_passing_year" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Percentage</label>
                                    <input type="text" name="hsc_percentage" required="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4>Bachlor Degree</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Course Name</label>
                                    <input type="text" name="bachlor_course_name" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>University</label>
                                    <input type="text" name="bachlor_board_university" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Passing Year</label>
                                    <input type="text" name="bachlor_passing_year" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Percentage</label>
                                    <input type="text" name="bachlor_percentage" required="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h4>Master Degree</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Course Name</label>
                                    <input type="text" name="master_course_name" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>University</label>
                                    <input type="text" name="master_board_university" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Passing Year</label>
                                    <input type="text" name="master_passing_year" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Percentage</label>
                                    <input type="text" name="master_percentage" required="" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Work Experience</div>

                    <div class="card-body">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Company Name</th>
                                            <th>Designation</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="addWorkExperienceRow">
                                        <tr>
                                            <td>
                                                <input type="text" name="comany_name[]" class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="designation[]" class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="from[]" class="form-control datepicker" readonly="">
                                            </td>
                                            <td>
                                                <input type="text" name="to[]" class="form-control datepicker" readonly>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-sm removeWorkExperienceRowBtn" id="">X</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                          <th colspan="5" class="text-right">
                                            <button type="button" data-field="comany_name[]" id="addWorkExperienceBtn" class="btn btn-success btn-sm">+ Add New</button>
                                          </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Languages Known</div>

                    <div class="card-body">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="language_know_hindi" class="languageKnown" data-lan="hindi" type="checkbox" value="Hindi"> Hindi
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="hindi_read" class="hindiCheckbox" type="checkbox" value="1" disabled=""> Read
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="hindi_write" class="hindiCheckbox" type="checkbox" value="1" disabled=""> Write
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="hindi_speak" class="hindiCheckbox" type="checkbox" value="1" disabled=""> Speak
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="language_know_english" class="languageKnown" data-lan="english" type="checkbox" value="English"> English
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="english_read" class="englishCheckbox" type="checkbox" value="1" disabled=""> Read
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="english_write" class="englishCheckbox" type="checkbox" value="1" disabled=""> Write
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="english_speak" class="englishCheckbox" type="checkbox" value="1" disabled=""> Speak
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="language_know_gujarati" class="languageKnown" data-lan="gujarati" type="checkbox" value="Gujarati"> Gujarati
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="gujarati_read" class="gujaratiCheckbox" type="checkbox" value="1" disabled=""> Read
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="gujarati_write" class="gujaratiCheckbox" type="checkbox" value="1" disabled=""> Write
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="gujarati_speak" class="gujaratiCheckbox" type="checkbox" value="1" disabled=""> Speak
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Technologies Experience</div>

                    <div class="card-body">
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="tech_know_php" class="techKnown" data-lan="php" type="checkbox" value="php"> PHP
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio3">
                                        <input type="radio" class="form-check-input phpKnow" id="radio3" name="php_know" value="Beginner" disabled="">Beginner
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio4">
                                        <input type="radio" class="form-check-input phpKnow" id="radio4" name="php_know" value="Mediator" disabled="">Mediator
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio5">
                                        <input type="radio" class="form-check-input phpKnow" id="radio5" name="php_know" value="Expert" disabled="">Expert
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="tech_know_mysql" class="techKnown" data-lan="mysql" type="checkbox" value="mysql"> Mysql
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio6">
                                        <input type="radio" class="form-check-input mysqlKnow" id="radio6" name="mysql_know" value="Beginner" disabled="">Beginner
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio7">
                                        <input type="radio" class="form-check-input mysqlKnow" id="radio7" name="mysql_know" value="Mediator" disabled="">Mediator
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio8">
                                        <input type="radio" class="form-check-input mysqlKnow" id="radio8" name="mysql_know" value="Expert" disabled="">Expert
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="tech_know_laravel" class="techKnown" data-lan="laravel" type="checkbox" value="laravel"> Laravel
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio9">
                                        <input type="radio" class="form-check-input laravelKnow" id="radio9" name="laravel_know" value="Beginner" disabled="">Beginner
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio10">
                                        <input type="radio" class="form-check-input laravelKnow" id="radio10" name="laravel_know" value="Mediator" disabled="">Mediator
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio11">
                                        <input type="radio" class="form-check-input laravelKnow" id="radio11" name="laravel_know" value="Expert" disabled="">Expert
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Preference </div>

                    <div class="card-body">
                       
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Preferred Location</label>
                                    <select class="form-control" name="preferred_location" required>
                                        <option>Ahmedabad</option>
                                        <option>Surat</option>
                                        <option>Rajkot</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Expected CTC</label>
                                    <input type="text" name="expected_ctc" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Current CTC</label>
                                    <input type="text" name="current_ctc" required="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Notice Period</label>
                                    <input type="text" name="notice_period" required="" class="form-control">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
