@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Basic Details
                    <a href="{{route('home')}}" class="btn btn-primary btn-sm pull-right" style="float: right">Back</a>
                </div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <td class="col-md-4">Name</td>
                            <td>{{$jobApplicationDB->first_name." ".$jobApplicationDB->last_name}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Designation</td>
                            <td>{{$jobApplicationDB->apply_designation}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Email</td>
                            <td>{{$jobApplicationDB->email}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Phone</td>
                            <td>{{$jobApplicationDB->phone_number}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Address</td>
                            <td>{{$jobApplicationDB->address_1.", ".$jobApplicationDB->address_2.", ".$jobApplicationDB->city.", ".$jobApplicationDB->state.". ".$jobApplicationDB->zipcode}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Gender</td>
                            <td>{{$jobApplicationDB->gender}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Relationship</td>
                            <td>{{$jobApplicationDB->relation_status}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Date Of Birth</td>
                            <td>{{$jobApplicationDB->dob}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Education Details</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Course Name</th>
                            <th>Name Of Board</th>
                            <th>Passing Year</th>
                            <th>Percentage</th>
                        </tr>
                        @foreach($jobApplicationDB->education as $eduction)
                            <tr>
                                <td>{{$eduction->course_name}}</td>
                                <td>{{$eduction->board_university}}</td>
                                <td>{{$eduction->passing_year}}</td>
                                <td>{{$eduction->percentage}} %</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Work Experience</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Company Name</th>
                            <th>Designation</th>
                            <th>From</th>
                            <th>To</th>
                        </tr>
                        @foreach($jobApplicationDB->workExperience as $workExperience)
                            <tr>
                                <td>{{$workExperience->comany_name}}</td>
                                <td>{{$workExperience->designation}}</td>
                                <td>{{$workExperience->from_date}}</td>
                                <td>{{$workExperience->to_date}} %</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Languages Known</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        @foreach($jobApplicationDB->languagesKnown as $languagesKnown)
                            <tr>
                                <td>{{$languagesKnown->language_name}}</td>
                                <td>
                                    {{($languagesKnown->is_read == 1) ? "Read" : "" }}
                                    {{($languagesKnown->is_write == 1) ? "Write" : "" }}
                                    {{($languagesKnown->is_speak == 1) ? "Speak" : "" }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Technologies Experience</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        @foreach($jobApplicationDB->technologiesExperience as $technologiesExperience)
                            <tr>
                                <td>{{ucwords($technologiesExperience->technology_name)}}</td>
                                <td>
                                    {{$technologiesExperience->technology_experience}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Preference</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <td class="col-md-4">Preferred Location</td>
                            <td>{{$jobApplicationDB->preferred_location}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Expected CTC</td>
                            <td>{{number_format($jobApplicationDB->expected_ctc, 2)}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Current CTC</td>
                            <td>{{number_format($jobApplicationDB->current_ctc, 2)}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4">Notice Period</td>
                            <td>{{$jobApplicationDB->notice_period}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
