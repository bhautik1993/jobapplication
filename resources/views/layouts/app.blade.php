<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'JobApplication') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        .error{
            color: red;
        }
    </style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'JobApplication') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if(in_array('login', Request::segments()))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/')}}">Job Application Form</a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                            <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif -->
                        @else
                            <li class="nav-item dropdown">
                                <a href="{{ route('home') }}">
                                    {{ Auth::user()->name }}
                                </a>
                                (<a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>)

                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>

  <script src="{{ asset('js/jquery.validate.js') }}" defer></script>
  <script src="{{ asset('js/form-validation.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#datatable1").dataTable({
            "columnDefs": [
                { "searchable": false, "targets": 5 }
              ]
        });
        $( ".datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: 0
        });
        $(document).on('click','.languageKnown',function(){
            var className = $(this).data("lan");
            console.log(className);
            if($(this).is(':checked'))
            {
                $("."+className+"Checkbox").removeAttr("disabled");
            }
            else{
                $("."+className+"Checkbox").prop('checked', false);
                $("."+className+"Checkbox").attr("disabled", 'true');
            }
        });

        $(document).on('click','.techKnown',function(){
            var className = $(this).data("lan");
            console.log(className);
            if($(this).is(':checked'))
            {
                $("."+className+"Know").removeAttr("disabled");
            }
            else{
                $("."+className+"Know").prop('checked', false);
                $("."+className+"Know").attr("disabled", 'true');
            }
        });

        $("#addWorkExperienceBtn").click(function(){
            $("#addWorkExperienceRow").append('<tr><td> <input type="text" name="comany_name[]" class="form-control"></td><td> <input type="text" name="designation[]" class="form-control"></td><td> <input type="text" name="from[]" class="form-control datepicker" readonly></td><td> <input type="text" name="to[]" class="form-control datepicker" readonly></td><td> <button class="btn btn-danger btn-sm removeWorkExperienceRowBtn" id="" type="button">X</button></td></tr>');

            $( ".datepicker" ).datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: 0
            });
        });

        $(document).on('click','.removeWorkExperienceRowBtn',function(){
            var totalRows = $("input[name='comany_name[]']").length;
            if(totalRows > 1){
                $(this).parent().parent().remove();
            }
            else{
                alert("At least one row required.");
            }
        });
    });
</script>
</body>
</html>
