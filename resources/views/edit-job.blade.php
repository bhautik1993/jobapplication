@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Edit Job Application</h2>
        </div>
    </div>
    <form method="POST" id="jobApplicationForm" action="{{ route('updateJob',$jobApplicationDB->id) }}">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(Session::has('success'))
                     <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">Basic Details 
                        <a href="{{route('home')}}" class="btn btn-primary btn-sm pull-right" style="float: right">Back</a>
                    </div>

                    <div class="card-body">
                       
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" required="" class="form-control" value="{{$jobApplicationDB->first_name}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" required="" class="form-control" value="{{$jobApplicationDB->last_name}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Designation</label>
                                    <input type="text" name="apply_designation" required="" class="form-control" value="{{$jobApplicationDB->apply_designation}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" required="" class="form-control" value="{{$jobApplicationDB->email}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone_number" required="" class="form-control" value="{{$jobApplicationDB->phone_number}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address 1</label>
                                    <input type="text" name="address_1" required="" class="form-control" value="{{$jobApplicationDB->address_1}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address 2</label>
                                    <input type="text" name="address_2" required="" class="form-control" value="{{$jobApplicationDB->address_2}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" required="" class="form-control" value="{{$jobApplicationDB->city}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <select class="form-control" name="state" required>
                                        <option {{($jobApplicationDB->state == 'Gujarat') ? "selected" : ""}}>Gujarat</option>
                                        <option {{($jobApplicationDB->state == 'Maharastra') ? "selected" : ""}}>Maharastra</option>
                                        <option {{($jobApplicationDB->state == 'Haryana') ? "selected" : ""}}>Haryana</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Zipcode</label>
                                    <input type="text" name="zipcode" required="" class="form-control" value="{{$jobApplicationDB->zipcode}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <div>
                                        <div class="form-check-inline">
                                          <label class="form-check-label" for="radio1">
                                            <input type="radio" class="form-check-input" id="radio1" name="gender" value="Male" {{($jobApplicationDB->gender == 'Male') ? "checked" : ""}}>Male
                                          </label>
                                        </div>
                                        <div class="form-check-inline">
                                          <label class="form-check-label" for="radio2">
                                            <input type="radio" class="form-check-input" id="radio2" name="gender" value="Female" {{($jobApplicationDB->gender == 'Female') ? "checked" : ""}}>Female
                                          </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>RelationShip Status</label>
                                    <select class="form-control" name="relation_status" required>
                                        <option {{($jobApplicationDB->relation_status == 'Single') ? "selected" : ""}}>Single</option>
                                        <option {{($jobApplicationDB->relation_status == 'Marride') ? "selected" : ""}}>Marride</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date Of Birth</label>
                                    <input type="text" readonly="" name="dob" required="" class="form-control datepicker" value="{{$jobApplicationDB->dob}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Education Details</div>

                    <div class="card-body">
                     @foreach($jobApplicationDB->education as $eduction)
                        <div class="row">
                            <div class="col-md-12">
                                <h4>{{$eduction->course_name}} Result</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Course Name</label>
                                    <input type="hidden" name="eddu_id[]" value="{{$eduction->id}}">
                                    <input type="text" name="course_name[]" required="" class="form-control" value="{{$eduction->course_name}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Name Of Board</label>
                                    <input type="text" name="board_university[]" required="" class="form-control" value="{{$eduction->board_university}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Passing Year</label>
                                    <input type="text" name="passing_year[]" required="" class="form-control" value="{{$eduction->passing_year}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Percentage</label>
                                    <input type="text" name="percentage[]" required="" class="form-control" value="{{$eduction->percentage}}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Work Experience</div>

                    <div class="card-body">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Company Name</th>
                                            <th>Designation</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="addWorkExperienceRow">
                                        @foreach($jobApplicationDB->workExperience as $workExperience)
                                        <tr>
                                            <td>
                                                <input type="text" name="comany_name[]" class="form-control" value="{{$workExperience->comany_name}}">
                                            </td>
                                            <td>
                                                <input type="text" name="designation[]" class="form-control" value="{{$workExperience->designation}}">
                                            </td>
                                            <td>
                                                <input type="text" name="from[]" class="form-control datepicker" readonly="" value="{{$workExperience->from_date}}">
                                            </td>
                                            <td>
                                                <input type="text" name="to[]" class="form-control datepicker" readonly value="{{$workExperience->to_date}}">
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-sm removeWorkExperienceRowBtn" id="">X</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                          <th colspan="5" class="text-right">
                                            <button type="button" data-field="comany_name[]" id="addWorkExperienceBtn" class="btn btn-success btn-sm">+ Add New</button>
                                          </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Languages Known</div>

                    <div class="card-body">
                     
                        <div class="row">
                            @php 
                                $hindiLan = $jobApplicationDB->languagesKnown->where('language_name','Hindi')->first();
                                $englishLan = $jobApplicationDB->languagesKnown->where('language_name','English')->first();
                                $gujaratiLan = $jobApplicationDB->languagesKnown->where('language_name','Gujarati')->first();
                            @endphp
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="language_know_hindi" class="languageKnown" data-lan="hindi" type="checkbox" value="Hindi" {{($hindiLan) ? "checked" : ""}}> Hindi
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="hindi_read" class="hindiCheckbox" type="checkbox" value="1" {{($hindiLan && $hindiLan->is_read) ? "checked" : ""}}  {{($hindiLan) ? "" : "disabled"}}> Read
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="hindi_write" class="hindiCheckbox" type="checkbox" value="1" {{($hindiLan && $hindiLan->is_write) ? "checked" : ""}} {{($hindiLan) ? "" : "disabled"}}> Write
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="hindi_speak" class="hindiCheckbox" type="checkbox" value="1" {{($hindiLan && $hindiLan->is_speak) ? "checked" : ""}} {{($hindiLan) ? "" : "disabled"}}> Speak
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="language_know_english" class="languageKnown" data-lan="english" type="checkbox" value="English" {{($englishLan) ? "checked" : ""}}> English
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="english_read" class="englishCheckbox" type="checkbox" value="1" {{($englishLan && $englishLan->is_read) ? "checked" : ""}} {{($englishLan) ? "" : "disabled"}}> Read
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="english_write" class="englishCheckbox" type="checkbox" value="1" {{($englishLan && $englishLan->is_write) ? "checked" : ""}} {{($englishLan) ? "" : "disabled"}}> Write
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="english_speak" class="englishCheckbox" type="checkbox" value="1" {{($englishLan && $englishLan->is_speak) ? "checked" : ""}} {{($englishLan) ? "" : "disabled"}}> Speak
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="language_know_gujarati" class="languageKnown" data-lan="gujarati" type="checkbox" value="Gujarati" {{($gujaratiLan) ? "checked" : ""}}> Gujarati
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="gujarati_read" class="gujaratiCheckbox" type="checkbox" value="1" {{($gujaratiLan && $gujaratiLan->is_read) ? "checked" : ""}} {{($gujaratiLan) ? "" : "disabled"}}> Read
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="gujarati_write" class="gujaratiCheckbox" type="checkbox" value="1" {{($gujaratiLan && $gujaratiLan->is_write) ? "checked" : ""}} {{($gujaratiLan) ? "" : "disabled"}}> Write
                                    </label>
                                    &nbsp;&nbsp;
                                    <label class="checkbox-inline">
                                      <input name="gujarati_speak" class="gujaratiCheckbox" type="checkbox" value="1" {{($gujaratiLan && $gujaratiLan->is_speak) ? "checked" : ""}} {{($gujaratiLan) ? "" : "disabled"}}> Speak
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Technologies Experience</div>

                    <div class="card-body">
                         @php 
                            $phpLan = $jobApplicationDB->technologiesExperience->where('technology_name','php')->first();
                            $mysqlLan = $jobApplicationDB->technologiesExperience->where('technology_name','mysql')->first();
                            $laravelLan = $jobApplicationDB->technologiesExperience->where('technology_name','laravel')->first();
                        @endphp
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="tech_know_php" class="techKnown" data-lan="php" type="checkbox" {{($phpLan) ? "checked" : ""}} value="php"> PHP
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio3">
                                        <input type="radio" class="form-check-input phpKnow" id="radio3" name="php_know" value="Beginner" {{($phpLan && $phpLan->technology_experience == "Beginner") ? "checked" : ""}} {{($phpLan) ? "" : "disabled"}}>Beginner
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio4">
                                        <input type="radio" class="form-check-input phpKnow" id="radio4" name="php_know" value="Mediator" {{($phpLan && $phpLan->technology_experience == "Mediator") ? "checked" : ""}} {{($phpLan) ? "" : "disabled"}}>Mediator
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio5">
                                        <input type="radio" class="form-check-input phpKnow" id="radio5" name="php_know" value="Expert" {{($phpLan && $phpLan->technology_experience == "Expert") ? "checked" : ""}} {{($phpLan) ? "" : "disabled"}}>Expert
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="tech_know_mysql" class="techKnown" data-lan="mysql" type="checkbox" {{($mysqlLan) ? "checked" : ""}} value="mysql"> Mysql
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio6">
                                        <input type="radio" class="form-check-input mysqlKnow" id="radio6" name="mysql_know" value="Beginner" {{($mysqlLan && $mysqlLan->technology_experience == "Beginner") ? "checked" : ""}} {{($mysqlLan) ? "" : "disabled"}}>Beginner
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio7">
                                        <input type="radio" class="form-check-input mysqlKnow" id="radio7" name="mysql_know" value="Mediator" {{($mysqlLan && $mysqlLan->technology_experience == "Mediator") ? "checked" : ""}} {{($mysqlLan) ? "" : "disabled"}}>Mediator
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio8">
                                        <input type="radio" class="form-check-input mysqlKnow" id="radio8" name="mysql_know" value="Expert" {{($mysqlLan && $mysqlLan->technology_experience == "Expert") ? "checked" : ""}} {{($mysqlLan) ? "" : "disabled"}}>Expert
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                      <input name="tech_know_laravel" class="techKnown" data-lan="laravel" type="checkbox" {{($laravelLan) ? "checked" : ""}} value="laravel"> Laravel
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio9">
                                        <input type="radio" class="form-check-input laravelKnow" id="radio9" name="laravel_know" value="Beginner" {{($laravelLan && $laravelLan->technology_experience == "Beginner") ? "checked" : ""}} {{($laravelLan) ? "" : "disabled"}}>Beginner
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio10">
                                        <input type="radio" class="form-check-input laravelKnow" id="radio10" name="laravel_know" value="Mediator" {{($laravelLan && $laravelLan->technology_experience == "Mediator") ? "checked" : ""}} {{($laravelLan) ? "" : "disabled"}}>Mediator
                                      </label>
                                    </div>
                                    <div class="form-check-inline">
                                      <label class="form-check-label" for="radio11">
                                        <input type="radio" class="form-check-input laravelKnow" id="radio11" name="laravel_know" value="Expert" {{($laravelLan && $laravelLan->technology_experience == "Expert") ? "checked" : ""}} {{($laravelLan) ? "" : "disabled"}}>Expert
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Preference </div>

                    <div class="card-body">
                       
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Preferred Location</label>
                                    <select class="form-control" name="preferred_location" required>
                                        <option {{($jobApplicationDB->preferred_location == 'Ahmedabad') ? "selected" : ""}}>Ahmedabad</option>
                                        <option {{($jobApplicationDB->preferred_location == 'Surat') ? "selected" : ""}}>Surat</option>
                                        <option {{($jobApplicationDB->preferred_location == 'Rajkot') ? "selected" : ""}}>Rajkot</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Expected CTC</label>
                                    <input type="text" name="expected_ctc" required="" class="form-control" value="{{$jobApplicationDB->expected_ctc}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Current CTC</label>
                                    <input type="text" name="current_ctc" required="" class="form-control" value="{{$jobApplicationDB->current_ctc}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Notice Period</label>
                                    <input type="text" name="notice_period" required="" class="form-control" value="{{$jobApplicationDB->notice_period}}">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
