 
$(document).ready(function(){
  
    jQuery.validator.addMethod("noSpace", function(value, element) { 
       return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");

    $("#jobApplicationForm").validate({
        rules: {
            first_name: {
                required: true,
                noSpace:true
            },
            last_name: {
                required: true,
                noSpace:true
            },
            apply_designation: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            phone_number: {
                required: true,
                number: true,
            },
            address_1: {
                required: true,
            },
            address_2: {
                required: true,
            },
            city: {
                required: true,
            },
            state: {
                required: true,
            },
            zipcode: {
                required: true,
                number: true,
            },
            dob: {
                required: true,
            },
            ssc_passing_year: {
                required: true,
                number: true,
            },
            hsc_passing_year: {
                required: true,
                number: true,
            },
            bachlor_passing_year: {
                required: true,
                number: true,
            },
            master_passing_year: {
                required: true,
                number: true,
            },
            ssc_percentage: {
                required: true,
                number: true,
            },
            hsc_percentage: {
                required: true,
                number: true,
            },
            bachlor_percentage: {
                required: true,
                number: true,
            },
            master_percentage: {
                required: true,
                number: true,
            },
            expected_ctc: {
                required: true,
                number: true,
            },
            current_ctc: {
                required: true,
                number: true,
            },
        },
        messages: {
            first_name: {
                required: "First name is required."
            },
            last_name: {
                required: "Last name is required."
            },
            apply_designation: {
                required: "Designation is required."
            },
            email: {
                required: "Email is required.",
                email: "Please enter valid email."
            }
            ,
            phone_number: {
                required: "Phone numer is required.",
                number: "Please enter only number."
            }
        }
    });
});