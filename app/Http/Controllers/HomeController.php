<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Validator;
use App\Models\JobApplication;
use App\Models\LanguagesKnown;
use App\Models\TechnologiesExperience;
use App\Models\WorkExperience;
use App\Models\Education;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jobApplicationDB = JobApplication::orderBy('id','DESC')->get();
        return view('home', compact('jobApplicationDB'));
    }

    public function show($id)
    {
        $jobApplicationDB = JobApplication::find($id);
        if($jobApplicationDB){
            return view('view-job', compact('jobApplicationDB'));
        }
        else{
            return redirect::to('/home');   
        }
    }

    public function edit($id)
    {
        $jobApplicationDB = JobApplication::find($id);
        if($jobApplicationDB){
            return view('edit-job', compact('jobApplicationDB'));
        }
        else{
            return redirect::to('/home');   
        }
    }

    public function update(Request $request, $id){
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'apply_designation' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'dob' => 'required',
            'expected_ctc' => 'required',
            'current_ctc' => 'required',
        ]);

        //dd($request->all());

        $jobApplicationDB = JobApplication::find($id);
        if($jobApplicationDB){
            $jobApplicationDB->first_name = $request->first_name;
            $jobApplicationDB->last_name = $request->last_name;
            $jobApplicationDB->apply_designation = $request->apply_designation;
            $jobApplicationDB->email = $request->email;
            $jobApplicationDB->phone_number = $request->phone_number;
            $jobApplicationDB->address_1 = $request->address_1;
            $jobApplicationDB->address_2 = $request->address_2;
            $jobApplicationDB->city = $request->city;
            $jobApplicationDB->state = $request->state;
            $jobApplicationDB->zipcode = $request->zipcode;
            $jobApplicationDB->gender = $request->gender;
            $jobApplicationDB->relation_status = $request->relation_status;
            $jobApplicationDB->dob = $request->dob;
            $jobApplicationDB->preferred_location = $request->preferred_location;
            $jobApplicationDB->expected_ctc = $request->expected_ctc;
            $jobApplicationDB->current_ctc = $request->current_ctc;
            $jobApplicationDB->notice_period = $request->notice_period;
            $jobApplicationDB->save();

            foreach($request->eddu_id as $key => $eduId){
            $educationDB = Education::find($eduId);
            if($educationDB){
                    $educationDB->application_id = $jobApplicationDB->id;
                    $educationDB->course_name = $request->course_name[$key];
                    $educationDB->board_university = $request->board_university[$key];
                    $educationDB->passing_year = $request->passing_year[$key];
                    $educationDB->percentage = $request->percentage[$key];
                    $educationDB->save();
                }
                else{
                    $educationDB = new Education();
                    $educationDB->application_id = $jobApplicationDB->id;
                    $educationDB->course_name = $request->course_name[$key];
                    $educationDB->board_university = $request->board_university[$key];
                    $educationDB->passing_year = $request->passing_year[$key];
                    $educationDB->percentage = $request->percentage[$key];
                    $educationDB->save();
                }
            }

            WorkExperience::where("application_id",$jobApplicationDB->id)->delete();
            foreach($request->comany_name as $key => $value){
                $workExperienceDB = new WorkExperience();
                $workExperienceDB->application_id = $jobApplicationDB->id;
                $workExperienceDB->comany_name = $value;
                $workExperienceDB->designation = $request->designation[$key];
                $workExperienceDB->from_date = $request->from[$key];
                $workExperienceDB->to_date = $request->to[$key];
                $workExperienceDB->save();
            }

            LanguagesKnown::where("application_id",$jobApplicationDB->id)->delete();
            if(isset($request->language_know_hindi)){
                $languagesKnownDB = new LanguagesKnown();
                $languagesKnownDB->application_id = $jobApplicationDB->id;
                $languagesKnownDB->language_name = $request->language_know_hindi;
                $languagesKnownDB->is_read = isset($request->hindi_read) ? $request->hindi_read : 0;
                $languagesKnownDB->is_write = isset($request->hindi_write) ? $request->hindi_write : 0;
                $languagesKnownDB->is_speak = isset($request->hindi_speak) ? $request->hindi_speak : 0;
                $languagesKnownDB->save();
            }

            if(isset($request->language_know_english)){
                $languagesKnownDB = new LanguagesKnown();
                $languagesKnownDB->application_id = $jobApplicationDB->id;
                $languagesKnownDB->language_name = $request->language_know_english;
                $languagesKnownDB->is_read = isset($request->english_read) ? $request->english_read : 0;
                $languagesKnownDB->is_write = isset($request->english_write) ? $request->english_write : 0;
                $languagesKnownDB->is_speak = isset($request->english_speak) ? $request->english_speak : 0;
                $languagesKnownDB->save();
            }

            if(isset($request->language_know_gujarati)){
                $languagesKnownDB = new LanguagesKnown();
                $languagesKnownDB->application_id = $jobApplicationDB->id;
                $languagesKnownDB->language_name = $request->language_know_gujarati;
                $languagesKnownDB->is_read = isset($request->gujarati_read) ? $request->gujarati_read : 0;
                $languagesKnownDB->is_write = isset($request->gujarati_write) ? $request->gujarati_write : 0;
                $languagesKnownDB->is_speak = isset($request->gujarati_speak) ? $request->gujarati_speak : 0;
                $languagesKnownDB->save();
            }

            TechnologiesExperience::where("application_id",$jobApplicationDB->id)->delete();
            if(isset($request->tech_know_php) && isset($request->php_know)){
                $techKnownDB = new TechnologiesExperience();
                $techKnownDB->application_id = $jobApplicationDB->id;
                $techKnownDB->technology_name = $request->tech_know_php;
                $techKnownDB->technology_experience = $request->php_know;
                $techKnownDB->save();
            }

            if(isset($request->tech_know_mysql) && isset($request->mysql_know)){
                $techKnownDB = new TechnologiesExperience();
                $techKnownDB->application_id = $jobApplicationDB->id;
                $techKnownDB->technology_name = $request->tech_know_mysql;
                $techKnownDB->technology_experience = $request->mysql_know;
                $techKnownDB->save();
            }

            if(isset($request->tech_know_laravel) && isset($request->laravel_know)){
                $techKnownDB = new TechnologiesExperience();
                $techKnownDB->application_id = $jobApplicationDB->id;
                $techKnownDB->technology_name = $request->tech_know_laravel;
                $techKnownDB->technology_experience = $request->laravel_know;
                $techKnownDB->save();
            }

            return Redirect::to('/home')->with('success', 'Job application updated successfully.');
        }
        else{
            return Redirect::to('/home')->with('error', 'Record not found!!');   
        }

    }

    public function destroy($id){
        $jobApplicationDB = JobApplication::find($id);
        if($jobApplicationDB){
            $jobApplicationDB->delete();
             return Redirect::to('/home')->with('success', 'Job application deleted successfully.');
        }
        else{
            return Redirect::to('/home')->with('error', 'Record not found!!');   
        }
    }
}
