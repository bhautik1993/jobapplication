<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Validator;
use App\Models\JobApplication;
use App\Models\LanguagesKnown;
use App\Models\TechnologiesExperience;
use App\Models\WorkExperience;
use App\Models\Education;

class JobController extends Controller
{
    public function index(){
    	return view('welcome');
    }

    public function store(Request $request){
    	$validatedData = $request->validate([
	        'first_name' => 'required',
	        'last_name' => 'required',
	        'apply_designation' => 'required',
	        'email' => 'required|email',
	        'phone_number' => 'required',
	        'address_1' => 'required',
	        'address_2' => 'required',
	        'city' => 'required',
	        'state' => 'required',
	        'zipcode' => 'required',
	        'dob' => 'required',
	        'ssc_passing_year' => 'required',
	        'hsc_passing_year' => 'required',
	        'bachlor_passing_year' => 'required',
	        'master_passing_year' => 'required',
	        'ssc_percentage' => 'required',
	        'hsc_percentage' => 'required',
	        'bachlor_percentage' => 'required',
	        'master_percentage' => 'required',
	        'expected_ctc' => 'required',
	        'current_ctc' => 'required',
	    ]);

    	$jobApplicationDB = new JobApplication();
    	$jobApplicationDB->first_name = $request->first_name;
    	$jobApplicationDB->last_name = $request->last_name;
    	$jobApplicationDB->apply_designation = $request->apply_designation;
    	$jobApplicationDB->email = $request->email;
    	$jobApplicationDB->phone_number = $request->phone_number;
    	$jobApplicationDB->address_1 = $request->address_1;
    	$jobApplicationDB->address_2 = $request->address_2;
    	$jobApplicationDB->city = $request->city;
    	$jobApplicationDB->state = $request->state;
    	$jobApplicationDB->zipcode = $request->zipcode;
    	$jobApplicationDB->gender = $request->gender;
    	$jobApplicationDB->relation_status = $request->relation_status;
    	$jobApplicationDB->dob = $request->dob;
    	$jobApplicationDB->preferred_location = $request->preferred_location;
    	$jobApplicationDB->expected_ctc = $request->expected_ctc;
    	$jobApplicationDB->current_ctc = $request->current_ctc;
    	$jobApplicationDB->notice_period = $request->notice_period;
    	$jobApplicationDB->save();

    	if($request->ssc_course_name){
    		$educationDB = new Education();
    		$educationDB->application_id = $jobApplicationDB->id;
    		$educationDB->course_name = $request->ssc_course_name;
    		$educationDB->board_university = $request->ssc_board_university;
    		$educationDB->passing_year = $request->ssc_passing_year;
    		$educationDB->percentage = $request->ssc_percentage;
    		$educationDB->save();
    	}
    	if($request->hsc_course_name){
    		$educationDB = new Education();
    		$educationDB->application_id = $jobApplicationDB->id;
    		$educationDB->course_name = $request->hsc_course_name;
    		$educationDB->board_university = $request->hsc_board_university;
    		$educationDB->passing_year = $request->hsc_passing_year;
    		$educationDB->percentage = $request->hsc_percentage;
    		$educationDB->save();
    	}
    	if($request->bachlor_course_name){
    		$educationDB = new Education();
    		$educationDB->application_id = $jobApplicationDB->id;
    		$educationDB->course_name = $request->bachlor_course_name;
    		$educationDB->board_university = $request->bachlor_board_university;
    		$educationDB->passing_year = $request->bachlor_passing_year;
    		$educationDB->percentage = $request->bachlor_percentage;
    		$educationDB->save();
    	}
    	if($request->master_course_name){
    		$educationDB = new Education();
    		$educationDB->application_id = $jobApplicationDB->id;
    		$educationDB->course_name = $request->master_course_name;
    		$educationDB->board_university = $request->master_board_university;
    		$educationDB->passing_year = $request->master_passing_year;
    		$educationDB->percentage = $request->master_percentage;
    		$educationDB->save();
    	}

    	foreach($request->comany_name as $key => $value){
    		$workExperienceDB = new WorkExperience();
    		$workExperienceDB->application_id = $jobApplicationDB->id;
    		$workExperienceDB->comany_name = $value;
    		$workExperienceDB->designation = $request->designation[$key];
    		$workExperienceDB->from_date = $request->from[$key];
    		$workExperienceDB->to_date = $request->to[$key];
    		$workExperienceDB->save();
    	}

    	if(isset($request->language_know_hindi)){
    		$languagesKnownDB = new LanguagesKnown();
    		$languagesKnownDB->application_id = $jobApplicationDB->id;
    		$languagesKnownDB->language_name = $request->language_know_hindi;
    		$languagesKnownDB->is_read = isset($request->hindi_read) ? $request->hindi_read : 0;
    		$languagesKnownDB->is_write = isset($request->hindi_write) ? $request->hindi_write : 0;
    		$languagesKnownDB->is_speak = isset($request->hindi_speak) ? $request->hindi_speak : 0;
    		$languagesKnownDB->save();
    	}

    	if(isset($request->language_know_english)){
    		$languagesKnownDB = new LanguagesKnown();
    		$languagesKnownDB->application_id = $jobApplicationDB->id;
    		$languagesKnownDB->language_name = $request->language_know_english;
    		$languagesKnownDB->is_read = isset($request->english_read) ? $request->english_read : 0;
    		$languagesKnownDB->is_write = isset($request->english_write) ? $request->english_write : 0;
    		$languagesKnownDB->is_speak = isset($request->english_speak) ? $request->english_speak : 0;
    		$languagesKnownDB->save();
    	}

    	if(isset($request->language_know_gujarati)){
    		$languagesKnownDB = new LanguagesKnown();
    		$languagesKnownDB->application_id = $jobApplicationDB->id;
    		$languagesKnownDB->language_name = $request->language_know_gujarati;
    		$languagesKnownDB->is_read = isset($request->gujarati_read) ? $request->gujarati_read : 0;
    		$languagesKnownDB->is_write = isset($request->gujarati_write) ? $request->gujarati_write : 0;
    		$languagesKnownDB->is_speak = isset($request->gujarati_speak) ? $request->gujarati_speak : 0;
    		$languagesKnownDB->save();
    	}

    	if(isset($request->tech_know_php) && isset($request->php_know)){
    		$techKnownDB = new TechnologiesExperience();
    		$techKnownDB->application_id = $jobApplicationDB->id;
    		$techKnownDB->technology_name = $request->tech_know_php;
    		$techKnownDB->technology_experience = $request->php_know;
    		$techKnownDB->save();
    	}

    	if(isset($request->tech_know_mysql) && isset($request->mysql_know)){
    		$techKnownDB = new TechnologiesExperience();
    		$techKnownDB->application_id = $jobApplicationDB->id;
    		$techKnownDB->technology_name = $request->tech_know_mysql;
    		$techKnownDB->technology_experience = $request->mysql_know;
    		$techKnownDB->save();
    	}

    	if(isset($request->tech_know_laravel) && isset($request->laravel_know)){
    		$techKnownDB = new TechnologiesExperience();
    		$techKnownDB->application_id = $jobApplicationDB->id;
    		$techKnownDB->technology_name = $request->tech_know_laravel;
    		$techKnownDB->technology_experience = $request->laravel_know;
    		$techKnownDB->save();
    	}

    	return Redirect::back()->with('success', 'Job application submitted successfully.');

    }
}
