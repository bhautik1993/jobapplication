<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\JobApplication;
use App\Models\LanguagesKnown;
use App\Models\TechnologiesExperience;
use App\Models\WorkExperience;
use App\Models\Education;

class JobApplication extends Model
{
    public function education(){
    	return $this->hasMany(Education::class, 'application_id', 'id');
    }

    public function workExperience(){
    	return $this->hasMany(WorkExperience::class, 'application_id', 'id');
    }

    public function languagesKnown(){
    	return $this->hasMany(LanguagesKnown::class, 'application_id', 'id');
    }

    public function technologiesExperience(){
    	return $this->hasMany(TechnologiesExperience::class, 'application_id', 'id');
    }
}
