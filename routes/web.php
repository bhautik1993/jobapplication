<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'JobController@index');
Route::post('/job-post', 'JobController@store')->name('jobPost');

Auth::routes();

Route::group(['middleware' => ['auth']], function()
{		
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/job-application/{id}', 'HomeController@show')->name('viewJob');
	Route::put('/job-application/{id}', 'HomeController@update')->name('updateJob');
	Route::delete('/job-application/{id}', 'HomeController@destroy')->name('deleteJob');
	Route::get('/job-application/{id}/edit', 'HomeController@edit')->name('editJob');
});
